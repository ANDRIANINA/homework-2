#!/usr/bin/env python
import math as mp
import numpy as np

if __name__ == "__main__":
    print "Welcome to the AIMS module"

def mean(numbers):
    try:
        return sum(numbers) / len(numbers)
    except ZeroDivisionError:
        return 0

def std(numbers):
    u=np.array(numbers)
    v=np.array(((u-mean(numbers))**2))
    sigma=mp.sqrt(sum(v)/len(numbers))
    return sigma



def avg_range(filenames):
    chars = []
    for files in filenames:
        open_file = open(files)
        for line in open_file:
            if line.startswith("Range:"):
                     chars.extend(line)
                     a=int(chars[-2])
        open_file.close()
    return a

        
