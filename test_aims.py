#!/usr/bin/env python 

from nose.tools import assert_equal 
from aims import *

def test_std1():
    numbers = [1,1,1,1,1]
    obs = std(numbers)
    exp = 0
    assert_equal(obs, exp)

def test_std2():
    numbers=[2,2,1,1]
    obs = std(numbers)
    exp = 0.0
    assert_equal(obs, exp) 

def test_std3():
    numbers=[1,1.5,1.5,1]
    obs = std(numbers)
    exp = 0.25
    assert_equal(obs, exp)    

def test_std4():
    numbers=[2,1,1,1]
    obs = std(numbers)
    exp = 0.0
    assert_equal(obs, exp) 




def test_avg1():
    filenames = ['data/bert/audioresult-00222']
    obs = avg_range(filenames)
    exp = 5
    assert_equal(obs, exp)

def  test_avg2():
    filenames=['data/bert/audioresult-00246']
    obs = avg_range(filenames)
    exp = 8
    assert_equal(obs, exp) 

def  test_avg3():
    filenames=['data/bert/audioresult-00355']
    obs = avg_range(filenames)
    exp = 7
    assert_equal(obs, exp)    

def test_avg4():
    filenames=['data/bert/audioresult-00246']
    obs = avg_range(filenames)
    exp = 8
    assert_equal(obs, exp) 

